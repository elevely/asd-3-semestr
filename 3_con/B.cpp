#define _USE_MATH_DEFINES
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <set>
#include <vector>
#include <queue>

const double FAULT = 1e-8;
class Point {
public:
    Point(double x, double y, double z): _x(x), _y(y), _z(z), _id(-1) {}
    Point(int id): _x(0), _y(0), _z(0), _id(id) {}
    Point(): _x(0), _y(0), _z(0), _id(-1) {}
    ~Point() {}

    double get_x() const { 
        return _x; 
    }
    double get_y() const { 
        return _y; 
    }
    double get_z() const { 
        return _z; 
    }
    double get_id() const { 
        return _id; 
    }

    void set_x(double value) {
        _x = value; 
    }
    void set_y(double value) {
        _y = value; 
    }
    void set_z(double value) {
        _z = value; 
    }

    void read_point() {
        std::cin >> _x >> _y >> _z;
    }

    void print_point() const {
        std::cout << "x: " << _x << " y: " << _y << " z: " << _z << std::endl;
    }

    bool operator== (Point& other) const{
        return (fabs(_x - other.get_x()) < FAULT && fabs(_y - other.get_y()) < FAULT && fabs(_z - other.get_z()) < FAULT);
    }

    Point operator- (const Point &other) {
        return Point(_x - other.get_x(), _y - other.get_y(), _z - other.get_z());
    }

    Point operator- () {
        return Point(-_x, -_y, -_z);
    }

    Point operator/ (const double value) {
        return Point(_x / value, _y / value, _z / value);
    }

    Point operator+ (const Point &other) {
        return Point(_x + other.get_x(), _y + other.get_y(), _z + other.get_z());
    }

    Point operator* (const double value) {
        return Point(_x * value, _y * value, _z * value);
    }

    Point operator* (const Point& other) const {
        return Point(_y * other.get_z() - _z * other.get_y(), _z * other.get_x() - _x * other.get_z(), _x * other.get_y() - _y * other.get_x()); 
    }

    Point operator= (const Point &other) {
        if (&other == this) {
            return *this;
        }
        _x = other.get_x();
        _y = other.get_y();
        _z = other.get_z();
        return Point(*this);
    }

    bool operator< (const Point& other) {
        if (_x < other.get_x()) {
            return true;
        } else if (_x > other.get_x()) {
            return false;
        } else {
            if (_y < other.get_y()) {
                return true;
            } else if (_y > other.get_y()) {
                return false;
            } else {
                return _z < other.get_z();
            }
        }
    }

    double scalar(const Point& other) const { 
        return _x * other.get_x() + _y * other.get_y() + _z * other.get_z();
    }

    double length() const { 
        return sqrt(_x * _x + _y * _y + _z * _z);
    }

    double cos_with_ox() {
        Point a_x(_x, _y, 0);
        if (length() && a_x.length()) {
            return scalar(a_x) / (length() * a_x.length());
        }
        return -1;
    }

private:
    double _x;
    double _y;
    double _z;
    int _id;
};

double dist (const Point& a, const Point& b) {
    return sqrt(pow(a.get_x() - b.get_x(), 2) + pow(a.get_y() - b.get_y(), 2) + pow(a.get_z() - b.get_z(), 2));
}

class Face {
public:
    Face() {
    }
    Face(size_t x, size_t y, size_t z, std::vector<Point>& dots) : _a(x), _b(y), _c(z) {
        while (_a > _b || _a > _c) {
            std::swap(_a, _b);
            std::swap(_b, _c);
        }
        _normal = (dots[_b] - dots[_a]) * (dots[_c] - dots[_a]);
    };
    ~Face() {}

    size_t get_a() const {
        return _a; 
    }
    size_t get_b() const {
        return _b; 
    }
    size_t get_c() const {
        return _c; 
    }
    
    bool operator< (Face other) const { 
        return _a < other.get_a() || (_a == other.get_a() && _b < other.get_b()) || (_a == other.get_a() && _b == other.get_b() && _c < other.get_c());
    }

private:
    size_t _a; // индексы в векторе точек
    size_t _b;
    size_t _c;

    Point _normal;
};

Point NULL_POINT;

struct Edge {
    Edge() {};
    Edge(size_t a, size_t b) : _a(a), _b(b) {};
    ~Edge() {}

    size_t get_a() const { 
        return _a; 
    }
    size_t get_b() const { 
        return _b; 
    }

    bool operator< (const Edge& e) const {
        return std::min(_a, _b) < std::min(e._a, e._b) || (std::min(_a, _b) == std::min(e._a, e._b) && std::max(_a, _b) < std::max(e._a, e._b));
    }

    void print_edge() {
        std::cout << "a: " << _a << " b: " << _b << std::endl; 
    }
    size_t _a;
    size_t _b;
};

class ConvexHull {
public:
    ConvexHull(std::vector<Point>& input_dots) {
        _dots = input_dots;
    }
    ~ConvexHull() {}
    std::set<Face> build_convex_hull(std::vector<Point>& dots);

    Edge find_first_edge();
    size_t find_face(size_t a, size_t b);


private:
    std::vector<Point> _dots;
    std::vector<Face> _faces;
};

Edge ConvexHull::find_first_edge() {
    size_t a = 0, b = 0;
    for (size_t i = 0; i < _dots.size(); ++i) {
        if (_dots[i].get_z() < _dots[a].get_z()) {
            a = i;
        }
    }
    if (a == b) 
        b = 1;
    for (size_t i = 0; i < _dots.size(); ++i) {
        if (i == a) {
            continue;
        }
        if ((_dots[i] - _dots[a]).cos_with_ox() > (_dots[b] - _dots[a]).cos_with_ox()) {
            b = i;
        }
    }
    return Edge(a, b); 
}

size_t ConvexHull::find_face(size_t a, size_t b) {
    size_t c = 0;
    Point normal = NULL_POINT;
    for (size_t i = 0; i < _dots.size(); ++i) {
        if (i == a || i == b) {
            continue;
        }
        if (normal == NULL_POINT || normal.scalar(_dots[i] - _dots[a]) > 0) {
            c = i;
            normal = (_dots[b] - _dots[a]) * (_dots[i] - _dots[a]);
        }
    }
    return c;
}

double angle(const Point& base, Point a, Point b) {
    a = a - base;
    b = b - base;
    double ccos = (a.get_x() * b.get_x() + a.get_y() * b.get_y() + a.get_z() * b.get_z()) / (sqrt(b.get_x() * b.get_x() + b.get_y() * b.get_y() + b.get_z() * b.get_z()) * sqrt(a.get_x() * a.get_x() + a.get_y() * a.get_y() + a.get_z() * a.get_z())); 
    return acos(ccos) * 180 / M_PI; // 180 градусов
}

std::set<Face> ConvexHull::build_convex_hull(std::vector<Point>& dots) {
    std::set<Face> hull;
    std::queue<Edge> q;
    std::set<Edge> used_edges;
    Edge first_edge = find_first_edge();
    q.push(first_edge);
    while(!q.empty()) {
        Edge cur_edge = q.front();
        q.pop();
        if (used_edges.find(cur_edge) != used_edges.end()) {
            continue;   
        }
        size_t c = find_face(cur_edge.get_a(), cur_edge.get_b());
        hull.insert(Face(cur_edge.get_a(), cur_edge.get_b(), c, dots));
        q.push(Edge(cur_edge.get_b(), c));
        q.push(Edge(c, cur_edge.get_a()));

        c = find_face(cur_edge.get_b(), cur_edge.get_a());
        hull.insert(Face(cur_edge.get_a(), c, cur_edge.get_b(), dots));

        q.push(Edge(cur_edge.get_b(), c));
        q.push(Edge(c, cur_edge.get_a()));
        used_edges.insert(cur_edge);
    }
    return hull;
}

int main() {
    size_t m;
    std::cin >> m;
    for(size_t j = 0; j < m; ++j) {
        size_t n;
        std::cin >> n;
        std::vector<Point> dots(n);
        for(size_t i = 0; i < n; ++i) {
            dots[i].read_point();
        }
        ConvexHull hull(dots);
        std::set<Face> result = hull.build_convex_hull(dots);
        std::cout << result.size() << std::endl;
        for (auto i : result) {
            std::cout << "3 " << i.get_a() << " " << i.get_b() << " " << i.get_c() << std::endl;
        }
    }
    return 0;
}
