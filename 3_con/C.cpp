#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>

const double FAULT = 1e-8;

class Point {
public:
    explicit Point(double x = 0, double y = 0): x(x), y(y) {}
    ~Point() {}

    double get_x() const { 
        return x; 
    }
    double get_y() const { 
        return y; 
    }

    void set_x(double value) {
        x = value; 
    }
    void set_y(double value) { 
        y = value; 
    }

    void read_point() {
        std::cin >> x >> y;
    }

    void print_point() const {
        std::cout << "x: " << x << " y: " << y << std::endl;
    }

    bool operator== (Point& other) {
        if (fabs(x - other.get_x()) < FAULT && fabs(y - other.get_y()) < FAULT) {
            return true;
        } else {
            return false;
        }
    }

    Point operator- (const Point &other) {
        return Point(x - other.get_x(), y - other.get_y());
    }

    Point operator- () {
        return Point(-x, -y);
    }

    Point operator/ (const double value) {
        return Point(x / value, y / value);
    }

    Point operator+ (const Point &other) {
        return Point(x + other.get_x(), y + other.get_y());
    }

    Point operator* (const double value) {
        return Point(x * value, y * value);
    }

    Point operator= (const Point &other) {
        if (&other == this) {
            return *this;
        }
        x = other.get_x();
        y = other.get_y();
        return Point(*this);
    }

    double operator% (const Point& a) {
        return x * a.get_y() - y * a.get_x();
    }

private:
    double x;
    double y;
};

double dist (const Point& a, const Point& b) {
    return sqrt(pow(a.get_x() - b.get_x(), 2) + pow(a.get_y() - b.get_y(), 2));
}

class Polygon {
public:
    Polygon(size_t m = 0): dots(m) {}
    ~Polygon() {}
    
    void read_polygon() {
        for (size_t i = 0; i < dots.size(); ++i) {
            dots[i].read_point();
        }
    }

    void add_point(const Point& a) {
        dots.push_back(a);
    } 

    Polygon operator- () {
        Polygon res(dots.size());
        for (size_t i = 0; i < dots.size(); ++i) {
            res.set_point(-(dots[i]), i);
        }
        return res;
    }

    size_t get_size() const {
        return dots.size();
    }
    Point& get_point(size_t index) {
        return dots[index];
    }

    void set_point (Point dot, size_t index) {
        dots[index] = dot;
    }

    void pop_point() {
        dots.pop_back();
    }

    void print_polygon() const {
        std::cout << "Printing polygon, polygon size is " << dots.size() << std::endl;
        for(auto i: dots) {
            i.print_point();
        }
        std::cout << "Printing ends" << std::endl;
    }

private:
    std::vector<Point> dots;
};

bool is_intersect(Polygon& n_polygon, Polygon& m_polygon) {
    size_t k = 0, l = 0;
    while ((n_polygon.get_point((k + 1) % n_polygon.get_size())).get_x() <= (n_polygon.get_point(k)).get_x()) {
        ++k;
    }
    while ((m_polygon.get_point((l + 1) % m_polygon.get_size())).get_x() <= (m_polygon.get_point(l)).get_x()) {
        ++l;
    }
    size_t i = 0, j = 0;
    Polygon res(1);
    Point p_point, q_point, current;
    double mul;
    while (i < n_polygon.get_size() || j < m_polygon.get_size()) {
        p_point = n_polygon.get_point((i + k + 1) % n_polygon.get_size()) - n_polygon.get_point((i + k) % n_polygon.get_size());
        q_point = m_polygon.get_point((j + l + 1) % m_polygon.get_size()) - m_polygon.get_point((j + l) % m_polygon.get_size());
        mul = p_point % q_point;
        if (fabs(mul) < FAULT) {
            ++i;
            ++j;
            current = p_point + q_point;
        } else if (mul < 0) {
            current = p_point;
            ++i;
        } else {
            current = q_point;
            ++j;
        }
        res.add_point(current);
    }
    
    res.set_point(n_polygon.get_point(k) + m_polygon.get_point(l), 0);
    for (size_t i = 1; i < res.get_size(); ++i) {
        res.set_point(res.get_point(i) + res.get_point(i - 1), i);
    }
    res.pop_point();
    double s;
    for (size_t i = 0; i < res.get_size(); ++i) {
        s = (res.get_point(i)) % res.get_point((i + 1) % res.get_size());
        if (s > 0) {
            return false;
        }
    }
    return true;
}


int main() {
    size_t n;
    std::cin >> n;
    Polygon n_polygon(n);
    n_polygon.read_polygon();
    size_t m;
    std::cin >> m;
    Polygon m_polygon(m);
    m_polygon.read_polygon();
    m_polygon = -m_polygon;
    if (is_intersect(n_polygon, m_polygon)) {
        std::cout << "YES";
    } else {
        std::cout << "NO";
    }
    return 0;
}