#include <iomanip>
#include <iostream>
#include <math.h>
#include <vector>
#include <utility>

const double FAULT = 1e-8;
const size_t NUMBER_OF_ITERATIONS = 200;

class Point {
public:
    Point(double x = 0, double y = 0, double z = 0): x(x), y(y), z(z) {}
    ~Point() {}

    double get_x() const { 
        return x;
    }
    double get_y() const { 
        return y; 
    }
    double get_z() const { 
        return z; 
    }

    void set_x(double value) { 
        x = value; 
    }
    void set_y(double value) { 
        y = value; 
    }
    void set_z(double value) { 
        z = value; 
    }

    void read_point() {
        std::cin >> x >> y >> z;
    }

    void print_point() const {
        std::cout << "x: " << x << " y: " << y << " z: " << z << std::endl;
    }

    bool operator== (Point& rv) const {
       return (fabs(x - rv.get_x()) < FAULT && fabs(y - rv.get_y()) < FAULT && fabs(z - rv.get_z()) < FAULT);
    }

    Point operator- (const Point &lv) {
        return Point(x - lv.get_x(), y - lv.get_y(), z - lv.get_z());
    }

    Point operator/ (const int i) {
        return Point(x / i, y / i, z / i);
    }

    Point operator+ (const Point &rv) {
        return Point(x + rv.get_x(), y + rv.get_y(), z + rv.get_z());
    }

    Point operator* (const int i) {
        return Point(x * i, y * i, z * i);
    }

    Point operator= (const Point &rv) {
        if (&rv == this) {
            return *this;
        }
        x = rv.get_x();
        y = rv.get_y();
        z = rv.get_z();
        return Point(*this);
    }

private:
    double x;
    double y;
    double z;
};

double dist (const Point& a, const Point& b) {
    double res = pow(a.get_x() - b.get_x(), 2) + pow(a.get_y() - b.get_y(), 2) + pow(a.get_z() - b.get_z(), 2);
    return sqrt(res);
}

Point& ternary_search(Point& left, Point& right, Point& from_other) {
    Point m1, m2;
    for (size_t j = 0; j < NUMBER_OF_ITERATIONS; ++j) {
        m1 = left + (right - left) / 3;
        m2 = right - (right - left) / 3;
        if (dist(from_other, m1) > dist(from_other, m2)) {
            left = m1;
        } else {
            right = m2;
        }
    }
    return left;
}

double find_shortest_distance(Point& first_begin, Point& first_end, Point& second_begin, Point& second_end) { //отрезки [first_begin,first_end] и [second_begin,second_end]
    Point from_first = first_begin, from_second = second_begin;
    Point prev_first = first_end, prev_second = second_end;
    for (size_t i; i < NUMBER_OF_ITERATIONS; ++i) {
        prev_first = from_first;
        from_first = ternary_search(first_begin, first_end, from_second);
        prev_second = from_second;
        from_second = ternary_search(second_begin, second_end, from_first);
    }
    return dist(from_first, from_second);
}

int main() {    
    Point a, b, c, d;
    a.read_point();
    b.read_point();
    c.read_point();
    d.read_point();
    std::cout << std::fixed << std::setprecision(8) << find_shortest_distance(a, b, c, d);
    return 0;
}