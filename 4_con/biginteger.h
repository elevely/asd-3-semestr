#include <vector>
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>

class BigInteger{
public:
	std::vector<int> vec;
	size_t length() const {
		return this->vec.size();
	}
	
	bool is_zero() const {
		return (this->length() == 1 && this->vec[0] == 0);
	}

	bool is_negative() const {
		return negative;
	}

	void change_sign(){
		negative = !negative;
	}

	void set_negative(bool value) {
		negative = value;
	}

	BigInteger() {
		this->set_negative(false);
		this->vec.resize(1, 0);
		this->vec[0] = 0;
	}

	friend std::ostream& operator <<(std::ostream & os, const BigInteger & x){
		os << x.toString();
		return os;
	}
	friend std::istream& operator >>(std::istream & is, BigInteger & x){
		std::string tmp;
		is >> tmp;
		x.vec.clear();
		if (tmp[0] == '-') { 
			tmp.erase(tmp.begin()); 
			x.set_negative(true);
		} else {
			x.set_negative(false);
		}
		x.vec.resize(tmp.length());
		for (size_t i = 0; i < tmp.length(); i++) {
			x.vec[i] = tmp[tmp.length() - i - 1] - '0';
		}
		if (x.is_zero()) {
			x.set_negative(false);
		}
		return is;
	}

	std::string toString() const{
		std::string ans = "";	
		for (int c : this->vec) {
			ans += (char)(c + (size_t)'0');
		}
		ans += (this->is_negative()) ? "-" : "";
 		std::reverse(ans);
		return ans;
	}


	BigInteger abs() const {
		BigInteger res = *this;
		res.set_negative(false);
		return res;
	}



	explicit operator bool() {
		return !this->is_zero();
	}

private:
	bool negative;
};

void trimZeros(BigInteger & y) {
    while (!y.vec.empty() && *(y.vec).rbegin() == 0) {
        y.vec.pop_back();
    }
    if (y.vec.empty()) {
        y.set_negative(true);
        y.vec.push_back(0);
    }
}

BigInteger operator - (const BigInteger & y) {
	if (y.is_zero())
		return y;
	BigInteger tmp = y;
	tmp.change_sign();
	return tmp;
}

bool operator == (const BigInteger & y, const BigInteger & x) {
	if ((y.is_negative() && !x.is_negative()) || (!y.is_negative() && x.is_negative()) || (y.length() != x.length())) 
		return false;
	for (size_t i = 0; i < y.length(); i++) {
		if (y.vec[i] != x.vec[i]) {
			return false;
		}
	}
	return true;
}

bool operator != (const BigInteger & y, const BigInteger & x) {
	return !(y == x);
}

bool operator < (const BigInteger & y, const BigInteger & x) {
	if (y.is_negative() && !x.is_negative()) {
		return true;
	}
	if (!y.is_negative() && x.is_negative()) {
		return false;
	} 
	bool true_val = true;
	if (y.is_negative()) {
		true_val = false;
	}
	if (y.length() < x.length()) {
		return true_val;
	}
	if (y.length() > x.length()) {
		return !true_val;
	}
	for (int i = (int)y.length() - 1; i >= 0; i--) {
		if(y.vec[i] < x.vec[i]) {
			return true_val; 
		} else if (y.vec[i] > x.vec[i]) {
			return !true_val;
		}
	}
	return false;
}

bool operator <= (const BigInteger & y, const BigInteger & x) {
	return (y < x) || (y == x);
}

bool operator > (const BigInteger & y, const BigInteger & x) {
	return !(y <= x);
}

bool operator >= (const BigInteger & y, const BigInteger & x) {
	return !(y < x);
}

BigInteger operator - (const BigInteger & y, const BigInteger & x);		

BigInteger operator + (const BigInteger & y, const BigInteger & x) {
	BigInteger ans;
	ans.vec.clear();
	if (y.is_negative() == x.is_negative()) {
		ans.set_negative(x.is_negative());
		int car = 0;
		for(size_t i = 0; i < std::max(y.length(), x.length()); i++){
			int l = (i < y.length()) ? y.vec[i] : 0;
			int r = (i < x.length()) ? x.vec[i] : 0;
			car += l + r;
			ans.vec.push_back(car % 10);
			car /= 10;
		}
		while(car) {
			ans.vec.push_back(car % 10); 
			car /= 10;
		}
		while(ans.length() > 1 && ans.vec.back() == 0) {
			ans.vec.pop_back();
		}
	} else {
		if (y.is_negative()) {
			ans = x - y.abs();
		} else {
			ans = y - x.abs();
		}
	}
	return ans;
}

BigInteger operator - (const BigInteger & y, const BigInteger & x) {
	BigInteger ans;
	ans.vec.clear();
	if (y.is_negative() == x.is_negative()) {
		if (x.abs() > y.abs()) {
			ans.set_negative(!x.is_negative());
		} else {
			ans.set_negative(x.is_negative());
		}
		const BigInteger *big = &y;
		const BigInteger *small = &x;
		if (y.abs() < x.abs()) {
			std::swap(big, small);
		}
		bool take = false;
		for (size_t i = 0; i < std::max(y.length(), x.length()); ++i) {
			int l = (int)(*big).vec[i];
			int r = (small->length() <= i) ? 0 : (int)(*small).vec[i];
			l -= r;
			if (take) {
				--l;
			}
			take = false;
			if (l < 0) {
				l += 10; 
				take = true;
			}
			ans.vec.push_back(l);
		}
	} else {
		if (y.is_negative()) {
			ans = y.abs() + x; 
			ans.set_negative(true);
		} else {
			ans = y + x.abs();
		}
	}
	while (ans.length() > 1 && ans.vec.back() == 0) {
		ans.vec.pop_back();
	}
	if (ans.is_zero()) {
		ans.set_negative(false);
	}
	return ans;
}

BigInteger operator / (const BigInteger & y, const BigInteger & x) {
	if (x.is_zero()) {
		exit(123);
	}
	if (y.is_zero()) {
		return y;
	}
	BigInteger ans, p = y.abs(), q;
	ans.vec.resize(1); 
	ans.vec[0] = 0;
	while(true) {
		q = x.abs();
		int pw = 0;
		while(q <= p) {
			q.vec.emplace(q.vec.begin(), 0);
			pw++;
		}
		pw--;
		if (pw == -1) {
			break;
		}
		q.vec.erase(q.vec.begin());
		p = p - q;
		BigInteger add;
		add.vec.resize(pw + 1);
		for (int i = 0; i < pw; i++) {
			add.vec[i] = 0;
		}
		add.vec[pw] = 1;
		ans = ans + add;
	}	
	ans.set_negative(y.is_negative() ^ x.is_negative());
	if(ans.is_zero()) {
		ans.set_negative(false);
	}
	return ans;
}



BigInteger operator * (const BigInteger & y, const BigInteger &x) {
    size_t length = x.length() + y.length() + 1;
    BigInteger c;
    c.vec.resize(length, 0);

    for (size_t i = 0; i < x.length(); ++i) {
        for (size_t j = 0; j < y.length(); ++j) {
            c.vec[i + j] += x.vec[i] * y.vec[j];
        }
    }

    for (size_t i = 0; i < length; ++i) {
        if (i < length - 1) {
            c.vec[i + 1] += c.vec[i] / 10;
        }
        c.vec[i] %= 10;
    }
    c.set_negative(x.is_negative() ^ y.is_negative());
    trimZeros(c);
    return c;
}

BigInteger operator % (const BigInteger & y, const BigInteger & x)  {
	BigInteger z = y / x;
	return y - (z * x);
}

BigInteger& operator += ( BigInteger & y, const BigInteger & x) {
	y = y + x;
	return y;
}

BigInteger& operator -= ( BigInteger & y, const BigInteger & x) {
	y = y - x;
	return y;
}

BigInteger& operator /= ( BigInteger & y, const BigInteger & x){
	y = y / x;
	return y;
}

BigInteger& operator %= ( BigInteger & y, const BigInteger & x){
	y = y % x;
	return y;
}
BigInteger operator *= ( BigInteger & y, const BigInteger & x) {
	y = y * x;
	return y;  
}

BigInteger& operator -- ( BigInteger & y) {
	y = y - BigInteger(1);
	return y;
}

BigInteger operator --( BigInteger & y, int){
	BigInteger res = y;
	y = y - BigInteger(1);
	return res;
}

BigInteger& operator ++( BigInteger & y ){
	y = y + BigInteger(1);
	return y;
}

BigInteger operator ++( BigInteger & y, int){
	BigInteger res = y;
	y = y + BigInteger(1);
	return res;
}

