#include <iostream>
#include <string>
#include <vector>


const std::vector<int> prefix_func(std::string s) {
	std::vector<int> prefix(s.length(), 0);
	for (int i = 1; i < s.length(); ++i) {
		int  k = prefix[i - 1];
		while(k > 0 && s[i] != s[k])
			k = prefix[k - 1];
		if (s[i] == s[k])
			++k;
		prefix[i] = k;
	}
	return prefix;
}


int main() {
	std::string pattern, line;
	std::cin >> pattern >> line;
	std::vector<int> res(pattern.length() + line.length() + 1);
	res = prefix_func(pattern + "#" + line);
	int pattern_length = pattern.length();
	for (int i = 0; i < res.size(); ++i) {
		if (res[i] == pattern_length) {
			std::cout << i - 2 * pattern_length << " "; 			
		}
	}
}