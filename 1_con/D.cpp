#include <iostream>
#include <vector>
#include <string>

const int ALPHABET_SIZE = 26;
const char MIN_SYMBOL = 'a';

struct Vertex {
	Vertex() = default;
	Vertex(int parent, char symbol) : parent(parent), symbol(symbol), terminals(0), pattern_indexes(0) {
		for (int i = 0; i < ALPHABET_SIZE; ++i) {
			next[i] = go[i] = -1;
		}
		link = -1;
	}

    int next[ALPHABET_SIZE];
    int go[ALPHABET_SIZE];

    int parent;  	
    int link;  		
    char symbol;  		
    bool terminal = false;  		
    std::vector<int> terminals;
    std::vector<int> pattern_indexes;   
};

class AhoCorasick{
public:
	AhoCorasick(size_t size) : trie(1), substr(size, 0) {
		trie[0].parent = trie[0].link = -1;
		for (int i = 0; i < ALPHABET_SIZE; ++i) {
			trie[0].next[i] = trie[0].go[i] = -1;
		}
	}

	void add_string(const std::string& pattern, int index);
	int get_link(int vert);
	int get_next_state(int vert, char ch);
	void count_entries(const std::string& str, std::vector<int> &sizes, std::vector<int> &shifts);
	std::vector<int> get_fast_link(int vert);
	void print_result(const std::vector<int> &sizes, const std::string &pattern, const std::string &str);
	std::vector<int> solve(std::string, std::string);

private:
	std::vector<Vertex> trie;
	std::vector<int> substr;
};

void AhoCorasick::count_entries(const std::string& str, std::vector<int> &sizes, std::vector<int> &shifts) {
	int cur = 0;
	for (int i = 0; i < str.size(); ++i) {
		char ch = str[i] - MIN_SYMBOL;
		cur = get_next_state(cur, ch);
		std::vector<int> fast_links = get_fast_link(cur);
		for (auto j : fast_links) {
			std::vector<int> result = trie[j].pattern_indexes;
			for(auto k : result) {
				if (i - shifts[k] - sizes[k] + 1 >= 0) {
					++substr[i - shifts[k] - sizes[k] + 1];
				}
			}
		}
	}
}

std::vector<int> AhoCorasick::solve(std::string str, std::string pattern) {
	std::vector<int> sizes(0), shifts(0);
	std::string cur_str = "";
	bool label = true;

    for (int i = 0; i < pattern.size(); ++i) {
        char ch = pattern[i];
        if (ch != '?') {
            cur_str += ch;
            if (label) {
                sizes.push_back(i);
            }
            label = false;
        } else {
            if (cur_str != "") {
                this->add_string(cur_str, sizes.size() - 1);
                shifts.push_back(cur_str.size());
                cur_str = "";
            }
            label = true;
        }
    }
    this->add_string(cur_str, sizes.size() - 1);
    shifts.push_back(cur_str.size());

	this->count_entries(str, sizes, shifts);
}

void AhoCorasick::add_string(const std::string& pattern, int index) {
	int vert = 0;
    for (auto i: pattern) {
        char ch = i - MIN_SYMBOL;
        if (trie[vert].next[ch] == -1) {
        	Vertex tmp(vert, ch);
			trie.push_back(tmp);
			trie[vert].next[ch] = trie.size() - 1;
		}
		vert = trie[vert].next[ch];
	}
	trie[vert].pattern_indexes.push_back(index);
	trie[vert].terminal = true;
}

int AhoCorasick::get_link(int vert) {
    if (trie[vert].link == -1) {                 
        if (!(vert * trie[vert].parent)) {
            trie[vert].link = 0;
        } else {
            trie[vert].link = get_next_state(get_link(trie[vert].parent), trie[vert].symbol);
        }
    }
    return trie[vert].link;
}

int AhoCorasick::get_next_state(int vert, char ch) { 
    if (trie[vert].go[(int)ch] == -1) {     
        if (trie[vert].next[(int)ch] != -1) {
            trie[vert].go[(int)ch] = trie[vert].next[(int)ch];
        } else { 
        	if (!vert) {
	            trie[vert].go[(int)ch] = 0; 
        	} else {
	            trie[vert].go[(int)ch] = get_next_state(get_link(vert), (int)ch);
	        }
	    }
    }
    return trie[vert].go[(int)ch];
}

std::vector<int> AhoCorasick::get_fast_link(int vert) { 
    if (trie[vert].terminals.size() > 0) {
		return trie[vert].terminals;
    }
	int cur = vert;
	while(cur) {
		if (trie[cur].terminal)
			trie[vert].terminals.push_back(cur);
		cur = get_link(cur);
	}
	return trie[vert].terminals;
}

void AhoCorasick::print_result(const std::vector<int> &sizes, const std::string &pattern, const std::string &str) {
	for (int i = 0; i < substr.size(); ++i) {
		if (substr[i] == sizes.size() && i + pattern.size() <= str.size()) {
			std::cout << i << " ";
		}
	}
}

int main() {
	std::string pattern, str;
	std::cin >> pattern >> str;
	AhoCorasick solver(str.size());
	std::vector<int> sizes = solver.solve(str, pattern);
	solver.print_result(sizes, pattern, str);
	return 0;
}