#include <iostream> 
#include <string>
#include <vector>

const std::string DEFAULT_STRING = "a";
const int ALPHABET_SIZE = 26;
const char MIN_SYMBOL = 'a';

std::string str_by_pref(const std::vector<int>  & pref) {
	if (pref.size() == 0) {
		return "";
	}
	std::string result = DEFAULT_STRING;
	for (size_t i = 1; i < pref.size(); ++i) {
		if (pref[i] != 0) {
			result += result[pref[i] - 1];
		} else {
			std::vector<bool> used(ALPHABET_SIZE, false);
			int ind = pref[i - 1];
			while(ind > 0) {
				used[result[ind] - MIN_SYMBOL] = true;
				ind = pref[ind - 1];
			}
			static char symbol = MIN_SYMBOL + 1;
			if (used[symbol - MIN_SYMBOL]) {
				++symbol;
			}
			result += symbol;
		}
	}
	return result;
}

std::vector<int> pref_by_z(std::vector<int> z) {
    std::vector<int> values (z.size(), 0);
    for (int i = 1; i < z.size(); ++i) {
    	for (int j = z[i] - 1; j >= 0; --j) {
      		if (values[i + j] > 0) {
      			break;
      		} else {
      			values[i + j] = j + 1;
      		}
        }
    }
  	return values;
}


int main() {
	std::vector<int> z;
	int value = 0;
	while (std::cin >> value) {
		z.push_back(value);
	}
	std::cout << str_by_pref(pref_by_z(z));
	return 0;
}