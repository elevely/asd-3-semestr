#include <iostream>
#include <string.h>
#include <stdio.h>
#include <vector>
#include <numeric>


const int ALPHABET_SIZE = 256;
const std::string ANSWER_WHEN_K_COMMON_SUBSTRING_DOESNT_EXIST = "-1";
const std::string FIRST_MINIMAL_SYMBOL = "#";
const std::string SECOND_MINIMAL_SYMBOL = "$";

class SuffArray {
public:
	SuffArray(const std::string& str);
	~SuffArray() {}
	void build_suffix_array();
	void lcp_build();
	const std::string find_k_common_substring(const long long k, const size_t first_string_length);

private:
	void preprocess();
	const std::string _input_string;
	size_t _size;
	size_t _number_of_classes;
	std::vector<size_t> _p; // отсортированные суффиксы, p[i] хранит позицию начала i-го суффикса.
	std::vector<size_t> _classes;
	std::vector<size_t> _lcp;

};

void SuffArray::preprocess() {
	std::vector<size_t> counting_array(ALPHABET_SIZE, 0);
	for(size_t i = 0; i < _size; ++i) {
		++counting_array[_input_string[i]];
	}
	std::partial_sum(counting_array.begin(), counting_array.begin() + ALPHABET_SIZE, counting_array.begin());
	for(size_t i = 0; i < _size; ++i) {
		_p[--counting_array[_input_string[i]]] = i;		
	}
	for(size_t i = 1; i < _size; ++i) {
		if (_input_string[_p[i]] != _input_string[_p[i - 1]]) {
			++_number_of_classes;
		}
		_classes[_p[i]] = _number_of_classes - 1;
	}
}

SuffArray::SuffArray(const std::string& str): _input_string(str) {
	// _input_string = str;
	_number_of_classes = 1;
	_size = _input_string.size();
	_p.assign(_size, 0);
	_classes.assign(_size, 0);
	_lcp.assign(_size, 0);

}

void SuffArray::build_suffix_array() {
	preprocess();
	std::vector<size_t> new_p(_size), new_classes(_size); // new_p содержит перестановку в порядке сортировки по вторым элементам пар
	std::vector<size_t> counting_array(_number_of_classes, 0);
	for (size_t substring_size = 0; substring_size < _size; substring_size += 2) {
		for (size_t i = 0; i < _size; ++i) {
			new_p[i] = _p[i] - substring_size;
			if (new_p[i] < 0) {
				new_p[i] += _size;
			}
		}
		counting_array.assign(_number_of_classes, 0);
		for (size_t i = 0; i < _size; ++i) {
			++counting_array[_classes[new_p[i]]];
		}
		std::partial_sum(counting_array.begin(), counting_array.end(), counting_array.begin());
		for (size_t i = _size - 1; i >= 0; --i) {
			_p[--counting_array[_classes[new_p[i]]]] = new_p[i];
		}
		new_classes[_p[0]] = 0;
		_number_of_classes = 1;
		for (size_t i = 1; i < _size; ++i) {
			if (_classes[_p[i]] != _classes[_p[i - 1]] || _classes[(_p[i] + substring_size) % _size] != _classes[(_p[i - 1] + substring_size) % _size]) {
				++_number_of_classes;
			}
			new_classes[_p[i]] = _number_of_classes - 1;
		}
		_classes = new_classes;
	}
}

void SuffArray::lcp_build() {
    std::vector<size_t> position(_size, 0);
    for (size_t i = 0; i < _p.size(); ++i) {
   	   	position[_p[i]] = i;
    }
    int size_common_prefix = 0;
    for (int i = 0; i < _size; ++i) {
    	if (size_common_prefix > 0) {
        	--size_common_prefix;  
    	}
      	if (position[i] == _size - 1) {
        	_lcp[_size - 1] = -1;
        	size_common_prefix = 0;
    	} else {
        	int j = _p[position[i] + 1]; // j - позиция начала лексикографически следующего суффикса
        	while (std::max(i + size_common_prefix, j + size_common_prefix) < _size && _input_string[i + size_common_prefix] == _input_string[j + size_common_prefix]) {
            	++size_common_prefix;
        	}
         	_lcp[position[i]] = size_common_prefix;
    	}
    }
}


const std::string SuffArray::find_k_common_substring(long long k, const size_t first_string_length) {
	std::string res = "";
	bool from_left_part = false;

	if (_p[0] < first_string_length) {
		from_left_part = true;
	}
	long long left = 0;
	long long right= 0;
	long long counter = 0;
	long long min_lcp = 0;
	for (long long i = 1; i < _input_string.size(); ++i) {
		if ((_p[i] < first_string_length && from_left_part) ||  (_p[i] >= first_string_length && !from_left_part)) {
			if (_lcp[i - 1] < min_lcp) {
				min_lcp = _lcp[i - 1];
			}
			continue;
		} else if ((_p[i] >= first_string_length && from_left_part) || (_p[i] < first_string_length && !from_left_part)) {
			if (_lcp[i - 1] < min_lcp) {
				min_lcp = _lcp[i - 1];
			}
			from_left_part = !from_left_part;
			counter += _lcp[i - 1] - min_lcp;
			min_lcp = _lcp[i - 1];
		}
		if (counter >= k) {
			left = _p[i];
			right = _p[i] + _lcp[i - 1] - (counter - k);
			for (long long i = left; i < right; ++i) {
				res += _input_string[i];
			}
			if (res == "") {
				return ANSWER_WHEN_K_COMMON_SUBSTRING_DOESNT_EXIST;
			}
			else {
				return res;
			}
		}
	}
	return ANSWER_WHEN_K_COMMON_SUBSTRING_DOESNT_EXIST;
}

int main() {
	std::string first_str, second_str;
	long long k;
	std::cin >> first_str >> second_str >> k;
	const std::string concatenate_string = first_str + SECOND_MINIMAL_SYMBOL + second_str + FIRST_MINIMAL_SYMBOL;
	SuffArray suff_arr(concatenate_string);
	suff_arr.build_suffix_array();
	suff_arr.lcp_build();
	std::string res = suff_arr.find_k_common_substring(k, first_str.size() + 1);
	std::cout << res;
}