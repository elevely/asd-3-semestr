#include <iostream>
#include <string.h>
#include <stdio.h>
#include <vector>
#include <numeric>
#include <algorithm>

const int ALPHABET_SIZE = 26;

class SuffArray {
public:
    explicit SuffArray(const std::string& str = ""): s(str), classes(1), size(s.size()), p(size), c(size), lcp(size) {} //изначально существует один класс
    void build_suff_arr();
    void lcp_build();
    void print_res() const;

private:
    void preprocess();

    std::string s;
    size_t classes;
    size_t size;
    std::vector<int> p;                 // отсортированные суффиксы, p[i] хранит позицию начала i-го суффикса.
    std::vector<int> c;                 // классы эквивалентности
    std::vector<int> lcp;

};

void SuffArray::preprocess() {          // (сортировка по подстрокам длины 1 и разбиение на классы
    std::vector<int> counting_array(ALPHABET_SIZE);
    std::iota(p.begin(), p.end(), 0);
    for(size_t i = 0; i < size; ++i) {
        ++counting_array[s[i] - 'a'];
    }
    std::partial_sum(counting_array.begin(), counting_array.begin() + ALPHABET_SIZE, counting_array.begin());

    for(size_t i = 0; i < size; ++i) {
        p[--counting_array[s[i] - 'a']] = static_cast<int>(i);
    }
    for(size_t i = 1; i < size; ++i) {
        if (s[p[i]] != s[p[i - 1]]) {
            ++classes;
        }
        c[p[i]] = static_cast<int> (classes) - 1;
    }
}

void SuffArray::print_res() const {
    int res = 0;
    for(int i: lcp) {
        res -= i;	
    }
    for(int i: p) {
        res += static_cast<int>(size) - i;
    }
    std::cout << res - 1;
}

void SuffArray::build_suff_arr() {
    preprocess();
    std::vector<int> pn(size); // содержит перестановку в порядке сортировки по вторым элементам пар
    std::vector<int> cn(size); // содержит новые номера классов эквивалентности
    std::vector<int> counting_array(size);
    for (size_t j = 0; (size_t)(1 << j) < size; ++j) {
        for (size_t i = 0; i < size; ++i) {
            pn[i] = p[i] - (1 <<j);
            if (pn[i] < 0)  {
                pn[i] += static_cast<int>(size);
            }
        }
        counting_array.assign(classes, 0);
        for (size_t i = 0; i < size; ++i) {
            ++counting_array[c[pn[i]]];
        }
        std::partial_sum(counting_array.begin(), counting_array.begin() + ALPHABET_SIZE, counting_array.begin());

        for (size_t i = size; i > 0; --i) {
            p[--counting_array[c[pn[i - 1]]]] = pn[i - 1];
        }
        cn[p[0]] = 0;
        classes = 1;
        for (size_t i = 1; i < size; ++i) {
            if (c[p[i]] != c[p[i - 1]] || c[(p[i] + (1 << j)) % size] != c[(p[i - 1] + (1 << j)) % size]) {
                ++classes;
            }
            cn[p[i]] = static_cast<int>(classes) - 1;
        }
        c = cn;
    }
}

void SuffArray::lcp_build() {
    std::vector<size_t> position(size); // position[] — массив, обратный суфмассиву
    for(size_t i = 0; i < size; ++i) {
        position[p[i]] = i;
    }
    int value = 0;
    for(size_t i = 0; i < size; ++i) {
        if (value > 0) {
            --value;
        }
        if (position[i] == size - 1) {
            lcp[size - 1] = -1;
            value = 0;
        } else {
            int j = p[position[i] + 1];
            while (std::max(static_cast<int>(i) + value, j + value) < static_cast<int>(size) && s[i + value] == s[j + value]) {
                ++value;
            }
            lcp[position[i]] = value;
        }
    }
}

int main() {
    std::string str;
    std::cin >> str;
    SuffArray myArr(str);
    myArr.build_suff_arr();
    myArr.lcp_build();
    myArr.print_res();
}